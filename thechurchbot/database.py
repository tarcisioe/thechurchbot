import sqlite3
from collections import defaultdict
from dataclasses import dataclass, field
from pathlib import Path
from typing import DefaultDict, Dict, List, Set, Tuple

from .song import Song
from .types import UserId, ChatId


@dataclass
class DataBase:
    '''Manages persistent data, that is, the subscribers, known songs
    and songs in a user's list
    '''
    path: Path
    connection: sqlite3.Connection = field(init=False)

    def __post_init__(self):
        self.connection = sqlite3.connect(self.path)

        # initialize tables if they're not already there
        sql_create_songs_table = ''' CREATE TABLE IF NOT EXISTS songs (
                                        id text PRIMARY KEY,
                                        artist text NOT NULL,
                                        title text NOT NULL
                                    ); '''
        sql_create_subs_table = ''' CREATE TABLE IF NOT EXISTS subscribers (
                                        chat_id integer PRIMARY KEY
                                    ); '''
        sql_create_lists_table = ''' CREATE TABLE IF NOT EXISTS lists (
                                        user_id integer NOT NULL,
                                        chat_id integer NOT NULL,
                                        PRIMARY KEY (user_id, chat_id)
                                    ); '''
        sql_create_songlists_table = ''' CREATE TABLE IF NOT EXISTS songlists (
                                            user_id integer NOT NULL,
                                            chat_id integer NOT NULL,
                                            song_id text NOT NULL,
                                            PRIMARY KEY (user_id, chat_id, song_id),
                                            FOREIGN KEY (song_id)
                                            REFERENCES songs (id) ON DELETE CASCADE,
                                            FOREIGN KEY (user_id, chat_id)
                                            REFERENCES lists(user_id, chat_id)
                                        ); '''

        with self.connection:
            self.connection.execute(sql_create_songs_table)
            self.connection.execute(sql_create_subs_table)
            self.connection.execute(sql_create_lists_table)
            self.connection.execute(sql_create_songlists_table)

    def insert_songs_batch(self, songs: List[Song]):
        '''Insert a batch of new songs in the database.

        Args:
            songs: list of songs to be added.
        '''
        songs_as_tuples = [
            (song.id, song.artist, song.title) for song in songs
        ]
        sql_insert_songs = 'INSERT INTO songs VALUES (?, ?, ?)'
        with self.connection:
            self.connection.executemany(sql_insert_songs, songs_as_tuples)

    def delete_songs_batch(self, songs: List[Song]):
        '''Delete a batch of songs from the database.

        Args:
            songs: list of songs to be deleted.
        '''
        songs_as_tuples = [
            (song.id, song.artist, song.title) for song in songs
        ]
        sql_delete_songs = '''DELETE FROM songs WHERE
                                id = ? AND
                                artist = ? AND
                                title = ?
                            '''
        with self.connection:
            self.connection.executemany(sql_delete_songs, songs_as_tuples)

    def insert_subscriber(self, chat_id: ChatId):
        '''Insert a chat_id in the subscribers table.

        Args:
            chat_id: id of the chat to be added
        '''
        with self.connection:
            self.connection.execute(
                'INSERT INTO subscribers VALUES (?)', (chat_id,)
            )

    def delete_subscriber(self, chat_id: ChatId):
        '''Remove a chat_id from the subscribers table.

        Args:
            chat_id: id of the chat to be removed
        '''
        with self.connection:
            self.connection.execute(
                'DELETE FROM subscribers WHERE chat_id=?', (chat_id,)
            )

    def songs(self) -> Dict[str, Song]:
        '''Get songs in the database

        Returns:
            A dict where the key is the song id as a string
            and the value is the song
        '''
        songs = self.connection.execute('SELECT * FROM songs')
        songs_dict = {
            id: Song(id, artist, title) for id, artist, title in songs
        }
        return songs_dict

    def subscribers(self) -> Set[ChatId]:
        '''Get set of subscribers from the database

        Returns:
            Set with id of chat subscribers
        '''
        subscribers = self.connection.execute('SELECT * FROM subscribers')
        return {id[0] for id in subscribers}

    def insert_in_list(self, user_id: UserId, chat_id: ChatId, song_id: str):
        '''Insert song in user's list, a new row in songlists table.
        It also creates a row in lists table in case it doesn't exists.

        Args:
            user_id: id of the user saving the song.
            chat_id: id of the chat where the song is being saved.
            song_id: song being saved.
        '''
        with self.connection:
            sql_insert_list_row = ''' INSERT OR IGNORE INTO lists VALUES (
                                            ?, ?
                                        ); '''
            self.connection.execute(sql_insert_list_row, (user_id, chat_id))

            sql_insert_songlists_row = ''' INSERT INTO songlists VALUES (
                                                ?, ?, ?
                                            ); '''
            self.connection.execute(
                sql_insert_songlists_row, (user_id, chat_id, song_id)
            )

    def delete_from_list(self, user_id: UserId, chat_id: ChatId, song_id: str):
        '''Remove relation list to song in songlists table.

        Args:
            user_id: id of the user removing the song.
            chat_id: id of the chat where the song is being removed.
            song_id: id of the song to be removed.
        '''
        sql_remove_songlists_row = ''' DELETE FROM songlists WHERE
                                        user_id = ? AND
                                        chat_id = ? AND
                                        song_id = ?
                                    '''
        with self.connection:
            self.connection.execute(
                sql_remove_songlists_row, (user_id, chat_id, song_id)
            )

    def all_lists(self) -> DefaultDict[Tuple[UserId, ChatId], List[Song]]:
        '''Gets all saved lists in the database.

        Returns:
            A DefaulDict maping user and chat id to a list of songs.
        '''
        sql_select_songlists = ''' SELECT
                                        songlists.user_id,
                                        songlists.chat_id,
                                        songs.id,
                                        songs.artist,
                                        songs.title
                                    FROM songlists INNER JOIN songs
                                    ON songlists.song_id = songs.id '''
        songlists_table = self.connection.execute(sql_select_songlists)

        songs = defaultdict(list)
        for user_id, chat_id, song_id, artist, title in songlists_table:
            songs[(user_id, chat_id)].append(Song(song_id, artist, title))
        return songs

    def get_lists_by_song(self, song_id: str) -> Set[Tuple[UserId, ChatId]]:
        '''Get in which lists a given song is

        Args:
            song_id: id of the song to be searched.
        Returns:
            A set os tuples indicating the id of the user who saved the song
            and the id of the chat where it was saved.
        '''
        sql_select_songlists = '''SELECT user_id, chat_id FROM songlists WHERE
                                    song_id = ? '''
        songlists_rows = self.connection.execute(
            sql_select_songlists, (song_id,)
        )
        return {tuple for tuple in songlists_rows}
