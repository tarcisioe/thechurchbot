import re
import unicodedata
from typing import Any, Dict, Iterable, List, NamedTuple, Tuple
from urllib import request

from lark import Lark, Tree


grammar = Lark('''
%import common.CNAME
%import common.ESCAPED_STRING
%import common._STRING_ESC_INNER

STRING: ESCAPED_STRING | "'" _STRING_ESC_INNER "'"

start: _list

_list: "[" object [ "," object ]* ","? "]"

object: "{" key_value [ "," key_value ]* ","? "}"

key_value: CNAME ":" STRING
''', parser='lalr')


def get_list() -> str:
    a = request.urlopen(
        'https://thechurchbar.com.br/static/js/main.7916b4e4.chunk.js'
    )
    js = a.read().decode()

    result, *_ = re.findall(r'songs:(\[[^\]]*\])', js)

    return result


def key_value(node: Tree) -> Tuple[str, str]:
    key, value = node.children
    return (
        str(key),
        str(value)
        .encode('utf8')
        .decode('unicode_escape')
        .strip('"\'')
    )


def js_object(node: Tree) -> Dict[str, str]:
    return dict(key_value(t) for t in node.children)


def js_list(node: Tree) -> List[Dict[str, Any]]:
    return [js_object(t) for t in node.children]


class Song(NamedTuple):
    id: str
    artist: str
    title: str

    @staticmethod
    def from_dict(d: Dict[str, str]) -> 'Song':
        return Song(
            id=d['id'],
            artist=d['artist'],
            title=d['song'],
        )

    def pretty(self):
        return self.id + ' | ' + self.artist + ' | ' + self.title


def normalised(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.lower()


def get_song_dict():
    song_dict = {
        o['id']: Song.from_dict(o) for o in js_list(grammar.parse(get_list()))
    }
    return song_dict


def search_songs(songs: Iterable[Song], needle: str) -> List[Song]:
    '''Search a song database for a given song or artist.

    Args:
        songs: a song dictionary.
        needle: the filter to apply on the song database.

    Returns:
        A list of songs that match the search terms.
    '''
    needle = needle.lower()

    return [
        song for song in songs
        if needle in song.artist.lower()
        or needle in song.title.lower()
        or needle == song.id
    ]
