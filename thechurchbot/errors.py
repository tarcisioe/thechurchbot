class TheChurchBotError(Exception):
    pass


class SongDoesNotExist(TheChurchBotError):
    pass


class SongAlreadyInList(TheChurchBotError):
    pass


class SongNotInList(TheChurchBotError):
    pass
