import asyncio
import os
from pathlib import Path

from carl import command
from telepot.aio import Bot
from telepot.aio.helper import Router
from telepot.aio.loop import MessageLoop

from .bot import ChurchBot
from .database import DataBase


def routes(church_bot: ChurchBot):
    routes = {
        x: getattr(church_bot, x)
        for x in
        [
            'search', 'start', 'help', 'add', 'remove', 'my_songs',
            'subscribe', 'unsubscribe',
        ]
    }
    routes[None] = church_bot.default

    router = Router(
        church_bot.read_command,
        routes,
    )

    return router.route


def get_db_path() -> Path:
    '''Check what's the path to the database.

    Returns:
        Database path.
    '''
    return (Path(os.environ['XDG_DATA_HOME'], __package__, 'database.db')
            if 'XDG_DATA_HOME' in os.environ else
            Path.home() / '.local/share' / __package__ / 'database.db'
            )


def ensure_dir(path: Path):
    '''Create parent folders for given path in case they don't exist.

    Args:
        path: path whose parents are to be ensured.
    '''
    dir = path.parent
    if not dir.is_dir():
        dir.mkdir(parents=True)


@command
async def main(token):
    db_path = get_db_path()
    ensure_dir(db_path)
    db = DataBase(db_path)
    church_bot = await ChurchBot.make(Bot(token), db)
    loop = asyncio.get_event_loop()
    loop.create_task(
        MessageLoop(church_bot.bot, routes(church_bot)).run_forever()
    )
    loop.create_task(church_bot.update_songs())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main.run_async())
    loop.run_forever()
