from typing import Any, DefaultDict, Dict, List, Tuple

from .song import Song


class UserId(int):
    pass


class ChatId(int):
    pass


UserSongLists = DefaultDict[Tuple[ChatId, UserId], List[Song]]
Message = Dict[str, Any]
