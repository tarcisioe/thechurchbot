import asyncio
import re
from collections import defaultdict
from dataclasses import dataclass, field
from textwrap import dedent
from typing import Any, Dict, List, Set, Tuple

from telepot.aio import Bot

from .database import DataBase
from .errors import SongAlreadyInList, SongDoesNotExist, SongNotInList
from .song import Song, get_song_dict, search_songs
from .types import ChatId, UserSongLists


COMMAND_RE = re.compile(r'/([^@\s]*)(?:@([^\s]*))?(?:\s+(.*))?', re.S)
MAX_MESSAGE_SIZE = 2048


@dataclass
class SongsManager:
    songs: Dict[str, Song] = field(default_factory=set)
    user_list: UserSongLists = field(
        default_factory=lambda: defaultdict(list)
    )

    def add(self, user_id, chat_id, song_id):
        '''Add a song to a user's personal list.

        Args:
            user_id: id of the user.
            chat_id: id of the chat where the add was requested
            song_id: id of the song to add.
        '''
        if song_id not in self.songs:
            raise SongDoesNotExist

        song = self.songs[song_id]
        if song in self.user_list[(user_id, chat_id)]:
            raise SongAlreadyInList

        self.user_list[(user_id, chat_id)].append(song)

    def remove(self, user_id, chat_id, song_id):
        '''Remove a song from a user's personal list.

        Args:
            user_id: id of the user.
            chat_id: id of the chat where the remove was requested
            song_id: id of the song to remove.
        '''
        for song in self.user_list[(user_id, chat_id)]:
            if song.id == song_id:
                self.user_list[(user_id, chat_id)].remove(song)
                return

        raise SongNotInList

    def update_songs(self, new_song_database) -> Tuple[List[Song], List[Song]]:
        '''Update the song database.

        Args:
            new_song_database: a dict to replace the current songs attribute

        Returns:
            A tuple with two items, the new songs and the removed songs,
            respectively
        '''
        new_songs = [
            song for song in new_song_database.values()
            if song not in self.songs.values()
        ]
        removed_songs = [
            song for song in self.songs.values()
            if song not in new_song_database.values()
        ]

        self.songs = new_song_database
        return new_songs, removed_songs


@dataclass
class ChurchBot:
    bot: Bot
    name: str
    songs_manager: SongsManager
    db: DataBase
    subscribers: Set[ChatId] = field(default_factory=set)

    @staticmethod
    async def make(bot: Bot, db: DataBase):
        name = (await bot.getMe())['username']
        songs = db.songs()
        lists = db.all_lists()
        songs_manager = SongsManager(songs, lists)
        subscribers = db.subscribers()

        return ChurchBot(bot, name, songs_manager, db, subscribers)

    async def search(self, message: Dict[str, Any], needle: str):
        '''Search the song database for a given song or artist.

        Args:
            message: a Message dict.
            needle: the filter to apply on the song database.
        '''
        filtered_songs = search_songs(self.songs_manager.songs.values(), needle)

        if filtered_songs:
            songs_description = '\n'.join(
                song.pretty() for song in filtered_songs
            )

            if len(songs_description) > MAX_MESSAGE_SIZE:
                answer = dedent('''\
                    O filtro encontrou mais resultados do que o \
                    que podemos mostrar. Seja mais específico ou \
                    procure diretamente pelo site.\
                    https://thechurchbar.com.br
                ''')
            else:
                answer = songs_description
        else:
            answer = 'Nenhuma música encontrada :('

        await self.bot.sendMessage(message['chat']['id'], answer)

    async def update_songs(self):
        while True:
            updated_songs = get_song_dict()
            new_songs, removed_songs = self.songs_manager.update_songs(
                updated_songs
            )
            if new_songs:
                self.db.insert_songs_batch(new_songs)

                for chat_id in self.subscribers:
                    await self.send_new_song_messages(chat_id, new_songs)
                    await asyncio.sleep(1)
            if removed_songs:
                for song in removed_songs:
                    for user_id, chat_id in self.db.get_lists_by_song(song.id):
                        self.songs_manager.remove(user_id, chat_id, song.id)
                        await self.bot.sendMessage(
                            chat_id, 'Some songs were deleted, check your lists!'
                        )
                self.db.delete_songs_batch(removed_songs)

            await asyncio.sleep(86400)

    async def send_new_song_messages(self, chat_id, songs):
        songs_list = [song.pretty() for song in songs]
        songs_description = '\n'.join(songs_list)

        if MAX_MESSAGE_SIZE > len(songs_description):
            await self.bot.sendMessage(chat_id, 'New songs were added')
            await self.bot.sendMessage(chat_id, songs_description)
        else:
            songs_description = '\n'.join(songs_list[:20])
            message = dedent('''\
                Lots of new songs were added! \
                Here\'s a preview of what\'s new:\n \
            ''')
            await self.bot.sendMessage(chat_id, message)
            await self.bot.sendMessage(chat_id, songs_description)

    async def subscribe(self, message, _):
        self.subscribers.add(message['chat']['id'])
        self.db.insert_subscriber(message['chat']['id'])
        await self.bot.sendMessage(message['chat']['id'], "Subscribed")

    async def unsubscribe(self, message, _):
        if message['chat']['id'] in self.subscribers:
            self.subscribers.remove(message['chat']['id'])
            self.db.delete_subscriber(message['chat']['id'])
        await self.bot.sendMessage(message['chat']['id'], "Unsubscribed")

    async def add(self, message, args):
        user_id = message['from']['id']
        chat_id = message['chat']['id']
        song_id = args

        try:
            self.songs_manager.add(user_id, chat_id, song_id)
        except SongDoesNotExist:
            return await self.bot.sendMessage(chat_id, 'Song does not exist')
        except SongAlreadyInList:
            return await self.bot.sendMessage(chat_id, 'Song already in list')

        self.db.insert_in_list(user_id, chat_id, song_id)
        await self.bot.sendMessage(chat_id, 'Song added')

    async def remove(self, message, args):
        user_id = message['from']['id']
        chat_id = message['chat']['id']
        song_id = args

        try:
            self.songs_manager.remove(user_id, chat_id, song_id)
        except SongNotInList:
            return await self.bot.sendMessage(chat_id, 'Song not in list')

        self.db.delete_from_list(user_id, chat_id, song_id)
        await self.bot.sendMessage(chat_id, 'Song removed')

    async def my_songs(self, message, _):
        user_id = message['from']['id']
        chat_id = message['chat']['id']

        songs_description = [
            song.pretty() for song in self.songs_manager.user_list[(user_id, chat_id)]
        ]
        songs_description = '\n'.join(songs_description)
        if songs_description:
            await self.bot.sendMessage(user_id, songs_description)
        else:
            await self.bot.sendMessage(user_id, 'No songs saved.')

    async def start(self, message, args):
        welcome = 'Welcome to The Church Bot!'
        await self.bot.sendMessage(message['chat']['id'], welcome)
        await self.help(message, args)

    async def help(self, message, _):
        help_msg = dedent('''\
            /help - Prints this help message.
            /search FILTER - Fetches musics that match FILTER.
            /add ID - Add song with id ID to your saved list in this chat.
            /remove ID - Remove song with id ID from your saved list in this chat.
            /saved_songs - Fetches your saved list
            /subscribe - Be warned when new musics are added.
            /unsubscribe - Remove yourself from the subscribers list.
            ''')
        await self.bot.sendMessage(message['chat']['id'], help_msg)

    def default(self, message, args):
        pass

    def read_command(self, message):
        match = COMMAND_RE.search(message['text'].strip())
        command, recipient, args = match.groups()

        if recipient is not None and recipient != self.name:
            return None

        return command, (args,)
