from thechurchbot.song import search_songs, Song

from .test_data import songs_dict as songs


def test_search():
    result = search_songs(songs.values(), 'Backstreet')
    assert result == [
        Song(id='4862', artist='Backstreet Boys', title='AS LONG AS YOU LOVE ME'),
        Song(id='9102', artist='Backstreet Boys', title='DROWNING'),
        Song(id='18104', artist='Backstreet Boys', title='EVERYBODY (BACKSTREET\'S BACK)'),
        Song(id='18063', artist='Backstreet Boys', title='I WANT IT THAT WAY'),
        Song(id='24114', artist='Backstreet Boys', title='INCOMPLETE'),
        Song(id='18736', artist='Backstreet Boys', title='INCONSOLABLE'),
        Song(id='9140', artist='Backstreet Boys', title='MORE THAN THAT'),
        Song(id='4956', artist='Backstreet Boys', title='QUIT PLAYING GAMES (WITH MY HEART)'),
        Song(id='18917', artist='Backstreet Boys', title='SHAPE OF MY HEART'),
        Song(id='4843', artist='Backstreet Boys', title='SHOW ME THE MEANING OF BEING LONELY'),
        Song(id='4976', artist='Backstreet Boys', title='THE CALL'),
        Song(id='4989', artist='Backstreet Boys', title='WE\'VE GOT IT GOING ON'),
    ]
