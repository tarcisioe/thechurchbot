import pytest

from thechurchbot.bot import (SongAlreadyInList, SongDoesNotExist,
                              SongNotInList, SongsManager)

from . import test_data


USER_ID = 1
CHAT_ID = 1
N_SONGS = 5
INVALID_ID = 1


@pytest.fixture
def songs_manager() -> SongsManager:
    '''Returns a song manager to be used on tests.

    Returns:
        A SongsManager instance with catalogue initialized.
    '''
    return SongsManager(test_data.songs_dict)


def test_add_songs(songs_manager):
    '''Test song addition in user's list
    '''
    expected_songs = test_data.songs_list[:N_SONGS]

    for song in expected_songs:
        songs_manager.add(USER_ID, CHAT_ID, song.id)

    user_list = songs_manager.user_list[(USER_ID, CHAT_ID)]
    assert len(user_list) == len(expected_songs)
    for song, expected_song in zip(expected_songs, user_list):
        assert song == expected_song


def test_add_multiple_times_same_song(songs_manager):
    '''Test if adding the same song to user's list causes exception
    '''
    song = test_data.songs_list[0]
    songs_manager.add(USER_ID, CHAT_ID, song.id)
    with pytest.raises(SongAlreadyInList):
        songs_manager.add(USER_ID, CHAT_ID, song.id)


def test_add_invalid_song(songs_manager):
    '''Test if adding an invalid song to user's list causes exception
    '''
    with pytest.raises(SongDoesNotExist):
        songs_manager.add(USER_ID, CHAT_ID, INVALID_ID)


def test_remove_songs(songs_manager):
    '''Test removing songs from user's list
    '''
    songs = test_data.songs_list[:N_SONGS]

    for song in songs:
        songs_manager.add(USER_ID, CHAT_ID, song.id)

    for song in songs:
        songs_manager.remove(USER_ID, CHAT_ID, song.id)
    assert not songs_manager.user_list[(USER_ID, CHAT_ID)]


def test_remove_song_not_in_list(songs_manager):
    '''Test if removing from user's list a song that's not there causes
     exception
     '''
    song = test_data.songs_list[0]
    with pytest.raises(SongNotInList):
        songs_manager.remove(USER_ID, CHAT_ID, song.id)


def test_remove_invalid_song(songs_manager):
    '''Test if removing an invalid song from user's list causes exception
    '''
    with pytest.raises(SongNotInList):
        songs_manager.remove(USER_ID, CHAT_ID, INVALID_ID)


def test_update_songs_with_new_songs():
    '''Test if updating songs databases returns new songs added
    '''
    old_songs = {song.id: song for song in test_data.songs_list[:N_SONGS]}
    songs_manager = SongsManager(old_songs)

    new_songs, removed_songs = songs_manager.update_songs(test_data.songs_dict)
    expected_new_songs = test_data.songs_list[N_SONGS:]
    assert new_songs == expected_new_songs
    assert not removed_songs

    new_songs, removed_songs = songs_manager.update_songs(test_data.songs_dict)
    assert not new_songs
    assert not removed_songs


def test_update_songs_with_fewer_songs(songs_manager):
    '''Test if updating songs returns empty list when songs are removed from
    the dict
    '''
    song = test_data.songs_list[0]
    new_songs_dict = {song.id: song}
    new_songs, removed_songs = songs_manager.update_songs(new_songs_dict)
    assert removed_songs == test_data.songs_list[1:]
    assert not new_songs
